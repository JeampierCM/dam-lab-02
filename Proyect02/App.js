import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';
import Message from './app/components/message/Message';
import Body from './app/components/body/Body';


const Provincias = [
  { id: 1, name: 'Arequipa' },
  { id: 2, name: 'Puno' },
  { id: 3, name: 'Cuzco' },
]

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      textValue: '',
      count: 0,
    };
  }
  
  changeTextInput = text => {
    console.log(text);
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({count: this.state.count + 1});
  };

  render() {
    return (
      <View style={styles.container}>
        <Message/>  
        <View style={styles.text}>
          <Text>Ingrese su edad</Text>
        </View>
        <TextInput 
          style={{height: 40,width: '65%', borderColor: 'gray', borderWidth: 1}}
          onChangeText={text1 => this.changeTextInput(text1)}
          value={this.state.textValue} />

        <Body textBody={'Texto de Body'} onBodyPress={this.onPress}/>

        <View style={styles.countContainer}>
          <Text style={styles.countText}>
            {this.state.count}</Text>
        </View>

      {Provincias.map((item => (
          <View>
            <Text>{item.name}</Text>
          </View>
        )))}

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
    fontSize: 20,
  },
});
